alias ls="exa"
alias reloadzsh="source ~/.oh-my-zsh/custom/aliases.zsh; source ~/.zshrc"
alias cargo="TERM=xterm cargo"
